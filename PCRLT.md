<!--
SPDX-FileCopyrightText: 2021-2022 embeach

SPDX-License-Identifier: CC-BY-SA-4.0

Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/pcrlt.git@master#PCRLT.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt.git@master#PCRLT.md
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding PCRLT.md

Note-PCRLT:CC-BY-SA-4.0:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"
-->
# Privacy-aware Content, Rights Holder and License Tracing (PCRLT) - Proposal for Best Practices
## 1 Version, Goal, Overview, Disclaimer
### 1.1 Version
Version: 0.4-SNAPSHOT  
Semantic versioning is used as specified in https://semver.org/ but without patch level version.  
For versions which do not contain the postfix "SNAPSHOT" applies: The same version number means the exact same text.  
For versions which contain the postfix "SNAPSHOT" the text may differ.
### 1.1 Goal of this document
This document is intended to provide guidance for privacy-aware, reliably and accurately documenting and managing rights holders and license terms for all content in an online Git repository. In addition, it represents a referenceable standard that should mitigate legal risks. There are already standards by SPDX and REUSE (see https://spdx.github.io/spdx-spec/ , https://reuse.software/spec/). The recommendations in this document are intended to complement them. (The current URL for referencing is: https://gitlab.com/emb_std/pcrlt ) 
### 1.2 Overview
The document contains four main parts "Personal Data", "Content Tracing", "Copyright Tracing", "License Tracing". Licenses are needed to be allowed to use content in the desired way. Content and Copyright holder tracing is needed to increase the trust that the licenses are granted by the real copyright holders. In the personal data section, an abbreviation or hash system is presented to allow referencing to copyright holders or authors while avoiding the processing of personal data. This will help to comply with EU-GDPR.
### 1.3 Disclaimer
**To date, there are no lawyers among the authors of this document. Therefore, NOTHING in this document should be interpreted as legal advice. Use the given information at your own risk. Although the authors have tried very hard to research and compare available options regarding copyright and licensing compliance, they may be completely wrong!!! Be warned!!!**
## 2 Basic Rules
* **1. Do not publish personal data (i.e. names, addresses) (without explicit consent of data subjects).**
* **2. Do not link content to identities (without explicit consent of data subjects).**
* **3. Do not remove copyright notices, if the content is not fully replaced**
* **4. If you add copyright notices (respecting rule 1,2,6), add a note why you think this is the case and that you have added them.**
* **5. If you add license notices, add a note why you think this is the case and that you have added them.**
* **6. As an exception to Rule 2, you may only link content in code in abbreviation format to a Git identity (without explicit consent) if the content is already part of a legally published Git repository and the content is already linked there to the Git identity by an authorship. This can be used to increase the copyright and license chain of trust.**

## 3 Personal Data
Since tracing content, authors, and copyright holders involves personal data, the privacy implications must be considered and appropriate mitigations put in place.
### 3.1 EU-GDPR, Source Code and Git
In order to protect personal data and comply with the EU GDPR (General Data Protection Regulation), the processing of personal data must be minimized and processing is prohibited unless the data subject has given informed consent or there is an overriding legitimate interest or other legal ground (for more information, visit https://eur-lex.europa.eu/eli/reg/2016/679/2016-05-04).  
For this reason, information from the Git history should not be used in content files without the consent of the data subjects. On the other hand, using content with unknown authors or rights holders carries legal risks. If the source code is distributed without Git history, this information is missing. A compromise could be to use the author information from the Git history, but only write an abbreviated form in the content files and provide a link to resolve the names (the linked repository must be public, otherwise the consent of the data subjects is required). This will increase the connection between content and authors, but will not spread the names and email addresses with each content file. The same is true for copyright holders, but usually it is not possible to determine copyright holders from the Git history. 
Without consent, adding author abbreviations should only be considered if the copyright holders are unknown or the copyright notice might be erroneous.
### 3.2 FAQ - EU-GDPR, Source Code and Git
##### 3.2.1 Is it legal to clone a public Git repository without explicit permission from all natural persons referenced in the Git history and republish the repository under a different URL?
The GDPR is applicable because while you might act privately when cloning, this is no longer the case when republishing and the repository will usually contain personal data (git name, email).  
However, processing and republication is possible if it can be assumed that the initial publication was authorized by the data subjects and the republication is compliant with copyright law (the processing is then usually based on legitimate interest). However, it is possible that at some point a data subject may request the removal of their Git identity in your repository. It is not clear whether this can be denied with reference to security implications if a rewrite of the Git history is necessary. It may be possible to avoid this in advance if the committer agrees to choose an identity that he will not insist on removing at a later date.
##### 3.2.2 Is it legal to add the author's name in a file without his/her explicit permission?
From a GDPR perspective, this should be avoided as he/she may have had a reason not to write his/her name or pseudonym in the file. However, if the Git history is publicly available, it seems acceptable to link to the Git repository and write an abbreviation that can only be resolved by using the Git repository. Please respect the author's right to informational self-determination and only use links that have already been legitimately published, and preferably only when doing so serves a legitimate purpose, such as increasing confidence in the correctness of copyright information.
##### 3.2.3 Is it legal to add copyright notices for copyright holders in a file header without their explicit permission?
From a GDPR perspective, this seems acceptable only in special situations. It seems to be acceptable to add the copyright holder in the file header if the copyright holder is already mentioned in a license file that should apply to the whole repository. It seems acceptable if the copyright holder is already listed as the author in the file, but to avoid false claims, you should add a note that you added the copyright holder and why. This also applies if an abbreviation is used. In both cases, use the exact same identity string that was used in the license file or authorship information. (For more information see also 3.2.2)
##### 3.2.4 Is it legal to link to the source of a copied content (e.g. licensed under MIT) without explicit permission?
From a GDPR perspective, this is problematic if the source URL is strongly associated with an identity of the author and he/she did not want to link the content to his/her identity. However, if the content is still publicly accessible under the source URL, the linking should be permissible.
##### 3.2.5 Is it legal to remove a copyright notice without explicit permission from this declared rights holder?
From a GDPR perspective this seems fine, but from a copyright perspective it could be problematic. You should leave copyright notices untouched except when the entire content is replaced.
### 3.3 Identities and Reference Ids 
To specify detailed statements about content, rights holder, and license traces, we need to create an efficient notation. We will use a simple entity-relationship model. This means that we will specify relationships between the following three main entities:

1. identities of natural or legal persons (e.g., identities of authors, identities of rights holders).
2. licenses
3. content snippets (e.g., commits/diffs).

To reference identities we will use Identity-Reference-Ids, to reference licenses we will use SPDX license expressions and to reference commits we will use filter expressions.

#### 3.3.1 Identity-Reference-Ids
We use the basic concept that a natural or legal person can have multiple identities. Each identity is a set of attributes such as abbreviation, name, and contact address (e.g. email or URL). Identities can be specified in a content file (e.g. `author: jane`) or via Git software (git.name="jane", git.email="jane@example.com"). 
Now we need to create an Identity-Reference-Id from a given identity to easily reference the identity without having to awkwardly repeat all the identity attributes every time we want to reference the given identity. Besides the goal of reducing repetition, the Identity-Reference-Id should also serve another goal: It should reduce the amount of personal data used in referencing, but be easier to associate with the identity than a plain number. The proposed solution is the following abbreviation/hashing technique for generating Identity-Reference-Ids from the name attribute of a given identity:
#### 3.3.2 Identity-Reference-Ids as Abbreviations/Hashes of Names
Suppose we want to create an Identity-Reference-Id that references a Git identity (git.name="jane"). The following formats are suggested (the later, the more specific):

1. `ja` (first n chars without dot, n>0. mostly ambiguous for n<=2, other matching examples: "john", "ja")
2. `ja.` (first n chars with dot, n>0. mostly ambiguous for n<=2, other matching examples: "john")
3. `j.4` (first n chars+number of chars, n>0. sometimes ambiguous, other matching examples: "john")
4. `j..e` (first n chars + last m chars, n,m>0. sometimes ambiguous, other matching example: "johanne")
5. `j..e.4` (first n chars + last m chars+ number of chars, n,m>0. mostly not ambiguous)  

Increase n,m to be more specific, but the more the person is identifiable by the hash (without access to the git history), the more the person's privacy rights could be violated.  
If git.name contains multiple words, e.g. "Jane Doe", the hash can be computed from the entire name or from parts or from all parts in sequence. The pattern can be chosen independently for each word:

* `J. D.` (applying pattern 2 on each word, separated by space. sometime ambiguous, other matching examples: "John Doe")
* `J.D.` (applying pattern 2 on each word, without space separator, other matching examples: "John Doe")
* `J..e.4` (applying pattern 5 on first word. mostly not ambiguous)
* `D..e.4` (applying pattern 5 on second word. mostly not ambiguous)
* `J.D..e.4` (applying pattern 2 on first word and pattern 5 on second word. mostly not ambiguous)

The space should be omitted only if the chosen pattern is still recognizable (e.g., J.5 should not be used for the name "Jane 5").  
We will call these abbreviations/hashes Identity-Reference-Ids for natural or legal persons.
#### 3.3.3 The Keyword: "resolve-git-identity", "resolve-identity"
Consider the following lines:
```
Note-PCRLT:authors:<text>
    2016-2018 j..n.4
    2015 j..e.4</text>
Note-PCRLT:j..n.4:resolve-git-identity:https://repository1
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
(For the keyword "authors" see section 5.2.2)
This means, for example, that the Identity-Reference-Id j..e.4 can be resolved in the (public) Git repository at the URL https://repository1, since there is a Git identity "jane"<janed@example.com> whose hash results in j..e.4 (and which has usually also contributed some commits as author or committer related to the content in question).  
Normally, the specified repository is the current development repository and there is an implicit mapping from the Identity-Reference-Id to commits committed/authored by the referenced Git identity. However, this is not strictly necessary as the main purpose is to resolve the name.  
The repositories can also be a logical expression like https://repository1 AND https://repository2 (if the id is resolvable in both) or "OR" (if the id is resolvable in at least one of them). Alternatively, the keyword can be used multiple times for a single Identity-Reference-Id which means the same as concatenating the URLs with "AND".  
If the URL does not contain a Git repository, the keyword `resolve-identity` can be used instead. To help with name resolving, additional text can be added (see 5.2.2).
#### 3.3.4 The Keyword: "specify-identity" and "map-to-git-identity"
Consider the following lines:
```
SPDX-FileCopyrightText: 2015 Jane Doe
Note-PCRLT:J..e.8:specify-identity:Jane Doe
Note-PCRLT:J..e.8:map-to-git-identity:j..e.4
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
Here we have an SPDX copyright notice that contains the name of the copyright holder "Jane Doe". Then a PCRLT identity "Jane Doe" is specified and associated with the Identity-Reference-Id "J..e.8". Then this Id is mapped to a Git identity referenced as "j..e.4" which can be resolved in https://repository1 (similar to the example before).
The example shows that we can specify identities in the file header, that we can reference identities from different sources (file header or remote git repository), and that we can map a file identity to a git identity. Of course, if the file identity has the same name and Identity-Reference-Id as the git identity, the mapping to the git identity can be omitted.  
If specify-identity keyword is used, it will be parsed as: name \<contact\> and the Identity-Reference-Id will be interpreted as abbreviation. (i.e. all characters before \< will be interpreted as part of name ignoring proceeding and ending whitespaces and characters within \< and \> will be interpreted as part of contact.). If  specify-identity-name,specify-identity-abbreviation,specify-identity-contact are used this behavior can be overwritten.  
For the keywords specify-identity-name,specify-identity-abbreviation,specify-identity-contact see also "5.2.5 Keyword: copyright-holder-..-policy"
#### 3.3.5 The Keyword: "personal-data-policy"
This is an experimental feature and more thought needs to be given to its use and usefulness before it can be recommended.  
Consider the following lines:
```
SPDX-FileCopyrightText: 2015 jane
Note-PCRLT:j..e.4:specify-identity-name:jane
Note-PCRLT:j..e.4:personal-data-policy:name-allowed
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
With the personal-data-policy the allowed personal data attributes are specified (together with specify-identity-name and specify-identity-abbreviation). This should prevent someone from modifying "2015 jane" to "2015 Jane Doe" even though this might be the more exact name. 
This also works with identities that are not based on Git:
```
SPDX-FileCopyrightText: 2015 Jane Doe
Note-PCRLT:J..e.8:specify-identity-name:Jane Doe
Note-PCRLT:j..e.8:personal-data-policy:name-allowed
Note-PCRLT:J..e.8:map-to-git-identity:j..e.4
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
The default assumption for the allowed personal data policy is:
```
SPDX-FileCopyrightText: 2015 j..e.4
Note-PCRLT:j..e.4:specify-identity-abbreviation:j..e.4
Note-PCRLT:j..e.4:personal-data-policy:abbreviation-allowed
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
This means that only abbreviations are allowed and the format 5. is used.
If even less personal data is desired, the abbreviation format can be changed:
```
SPDX-FileCopyrightText: 2015 j
Note-PCRLT:j:specify-identity-abbreviation:j
Note-PCRLT:j:personal-data-policy:abbreviation-allowed
Note-PCRLT:j:resolve-git-identity:https://repository1
```
However, this is probably ambiguous.

The keywords specify-identity-name, specify-identity-abbreviation, specify-identity-contact correspond to the keywords name-allowed, abbreviation-allowed, contact-allowed. (If several keywords are to be applied, they can be separated by commas) (see also the parsing rules from specify-identity in 3.3.4)
## 4 Content Tracing
### 4.1 The keywords: "src-trace", "src-trace-repository"
In addition to the Git history, it is sometimes helpful to know the URLs or the repository URLs that already contained parts of the content. This is especially helpful if the Git history has been rewritten in the current repository and the older repository is (still) public.
Since adding could have privacy implications, some weighing is required.
```
Note-PCRLT:src-trace-repository:2020:public:https://repository2
Note-PCRLT:src-trace-repository:2015:public:https://repository1
```
```
Note-PCRLT:src-trace:2020:public:https://somesite2
Note-PCRLT:src-trace:2015:public:https://somesite1
```
The keywords public, internal, private should be used. The URL should be a valid URL or a VCS location as specified by the SPDX 2.3 PackageDownloadLocation. If no URL is available but the project should still be mentioned, free text preceded by "no-url" can be used instead.
There are two possible annotation formats. The example above shows the list view where the annotation lines should be sorted by year ("src-trace" and "src-trace-repository" annotations can be mixed). The year should usually refer to the year of the last change before use. It is also possible to add multiple annotation lines of the same repository if a renaming has occurred or if the first change should also be logged (see example below). It is also allowed to log each year in which changes have occurred.
```
Note-PCRLT:src-trace-repository:2020:public:https://repo2#FilePath3
Note-PCRLT:src-trace-repository:2015:public:https://repo1#FilePath2
Note-PCRLT:src-trace-repository:2014:public:https://repo1#FilePath1
Note-PCRLT:src-trace-repository:2012:public:https://repo1#FilePath1
```
The second option is the graph view.
The content history can be modeled as a graph, where the nodes are snippets, the start node is the most recent snippet, and the relation between node A and B is "A uses parts of B and at least some of those parts are used in the start node", where "use" usually means that some kind of copyright-relevant copying process was involved. Consider the following example:
```
Note-PCRLT::src-trace-repository:2020:public:Url4
Note-PCRLT:-:src-trace-repository:2016:public:RepoUrl2#FilePath2
Note-PCRLT:--:src-trace-repository:2015:public:RepoUrl2#FilePath1
Note-PCRLT:---:src-trace-repository:2012:public:Url1
Note-PCRLT:-:src-trace-repository:2016:public:Url3:
```
The start node reference is "2020:Url4". In this snippet, two other snippets were used: "2016:RepoUrl2#FilePath2" and "2016:Url3". In the first branch, a renaming was involved and another snippet was used in the history "2012:Url1" (all URLs are supposed to be repository URLs in this example).

## 5 Rights-Holder Tracing (Copyright Notices)
In the following, the term copyright shall also include related rights. This could change in later versions.
### 5.1 Purpose
* In open source projects, specifying the copyright holder is helpful for both the copyright holder and the project.
* For the copyright holder:
    * Copyright holders do not need to be mentioned to hold the copyright for a content. In the event of a dispute, however, they might have to prove that they are the holder and it might help if they are already specified.
* For the project:
    * When a copyright holder A claims that his/her copyright has been infringed, the following questions could be helpful to answer:
        * **1.** Is A the true copyright holder of the content in question?
        * **2.** If so, is the true copyright holder linked to the content in the repository, along with a license?
        * **3.** If so, did the true copyright holder grant this license?
        * **4.** If so, were the license conditions met and did the license grant the necessary permissions?
    * Specifying the copyright holders is helpful to solve such issues. But even if a project does not have to defend against a claim, there are two other important reasons to track copyright holdership:
        * **1.** If the project wants to relicense the project, usually all copyright holder have to be asked.
        * **2.** If the project wants to enforce the copyright of its contributors (for example if a GPLv3 license has been violated by a company by not providing the source code for their distributions) they need the copyright holders to join forces.

### 5.2 File-based Copyright Holder Specification
#### 5.2.1 Format for file-based copyright holder specifications
* Examples in the recommended format:
    * `SPDX-FileCopyrightText: 2015-2020 Jane Doe <jane.doe@example.com>`
    * `SPDX-FileCopyrightText: 2015-2020 Future Inc. <future-inc.com>`
    * `SPDX-FileCopyrightText: 2015 Jane`
    * `SPDX-FileCopyrightText: 2015 J..e.4`
* Date:
    * Start year: First modified date.
    * End year: Last modified date.
    * If start year == end year, use start year only.
* Name:
    * Full name or pseudonym or abbreviation. (Some projects might have more restrictive requirements)
* Contact:
    * A way to contact the entity. E-mail, website or post address. This is optional. (Some projects might have more restrictive requirements)

#### 5.2.2 The Keywords: "authors", "author", "author-background"
Example:
```
Note-PCRLT:authors:<text>
    2016-2018 j..n.4
    2015 j..e.4</text>
Note-PCRLT:j..n.4:resolve-git-identity:https://repository1
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
or
```
Note-PCRLT:j..n.4:author:2016-2018 j..n.4
Note-PCRLT:j..e.4:author:2015 j..e.4
Note-PCRLT:j..n.4:resolve-git-identity:https://repository1
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
Authors are the people who originally wrote the contents of the file. If the file was partially generated by software, hard-coded portions or snippets by the software user may have been incorporated, so the authors of those snippets would then need to be listed as well. Authors whose content has been completely replaced should not be listed. However, they may be listed if their ideas have been retained in the replaced content. The conditions to be listed as an author are less than those to be listed as a copyright holder. The origin of all snippets (e.g., from other files) must be evaluated to identify all authors.  
The following shows an example usage of the keyword `author-background`.
```
Note-PCRLT:j..n.4:author:2016-2018 j..n.4
Note-PCRLT:j..e.4:author:2015 j..e.4
Note-PCRLT:D..Z.17:author:2010 Developers of XYZ
Note-PCRLT:D..Z.17:author-background: File was generated with XYZ version 1.0. Some content might be copyrighted
Note-PCRLT:j..n.4:resolve-git-identity:https://repository1
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
Note-PCRLT:D..Z.17:resolve-identity:identities might be resolvable in https://repository3
```
#### 5.2.3 The Keyword: "copyright-background"
The keyword should be used to indicate by whom the copyright notice was added and why he/she thinks it is correct.  
Example:
```
SPDX-FileCopyrightText: 2015 Jane Doe
Note-PCRLT:J..e.8:specify-identity-name:Jane Doe
Note-PCRLT:j..e.8:copyright-background:Copyright "2015 Jane Doe" was copied from the reposity MIT license file.
```
or
```
SPDX-FileCopyrightText: 2015 j..e.4
Note-PCRLT:j..e.4:copyright-background:Copyright "2015 j..e.4" was added by j..n.4 because according to j..e.4 no employment or copyright-assignement was involved.
Note-PCRLT:j..n.4:resolve-git-identity:https://repository1
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
#### 5.2.4 The Keyword: "map-copyright-holder-to-git-commits" and "map-author-to-git-commits"
Example:
```
SPDX-FileCopyrightText: 2015 j..e.4
Note-PCRLT:j..e.4:map-copyright-holder-to-git-commits:all commits by j..e.4
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
This would mean all commits committed or authored by j..e.4 that contribute to the content of the file.  
To determine which commits contribute to the content of the file, we give the following example:

1. author a1 creates file f1 with commit c1
2. author a2 creates file f2 with commit c2
3. author a3 merges files f1 and f2 and creates f3 with commit c3

Now c1,c2,c3 all contribute to the contents of file f3, but a1 may have performed other commits that have no effect on the contents of f3. These other commits are excluded.  
If it is difficult to interpret which commits should be included, the following expression can help, for example:
```
SPDX-FileCopyrightText: 2015 j..e.4
Note-PCRLT:j..e.4:map-copyright-holder-to-git-commits:all commits by j..e.4 (incl. commit 1234566 2015-05-05 and incl. commits related to file f1)
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
References to commit hashes should be avoided as they might change during a Git history rewrite.  
Sometimes it is helpful to also map authors to git commits, then "map-author-to-git-commits" can be used in the same way.
#### 5.2.5 The Keyword: "copyright-holder-add-remove-modify-group-policy"
This is an experimental feature and more thought needs to be given to its use and usefulness before it can be recommended.  
Normally, the policy for copyright notices is: Do not touch copyright notices other than your own.  
If you want to allow modifications, you can create a policy. In the policy you can allow 4 permissions: add, remove, modify, group.  
Example:  
Suppose the following lines are given:  
```
Note-PCRLT:j..e.4:specify-identity-name:jane
Note-PCRLT:j..e.4:personal-data-policy:name-allowed
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
Note-PCRLT:j..e.4:copyright-holder-add-remove-modify-group-policy:add-allowed,remove-allowed,modify-allowed,group-allowed,conditions:personal-data-policy
```
Then the following copyright notice might be added (remember to add a background note):
```
SPDX-FileCopyrightText: 2015 jane
Note-PCRLT:j..e.4:specify-identity-name:jane
Note-PCRLT:j..e.4:copyright-background:Copyright "2015 jane" was added by j..n.4 based on policy "add-allowed"
    because according to j..e.4 no employment or copyright-assignement was involved. 
Note-PCRLT:j..e.4:personal-data-policy:name-allowed
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
Note-PCRLT:j..e.4:copyright-holder-add-remove-modify-group-policy:add-allowed,remove-allowed,modify-allowed,group-allowed,conditions:personal-data-policy
```
group-allowed means that the copyright holder permits to be subsumed in a grouping copyright notice like:
```
SPDX-FileCopyrightText:Developers of Project XYZ
```
The "conditions:personal-data-policy" is redundant, personal-data-policy applies in any case. This is just to avoid mistakes.  
If no permission should be granted (this is the default) the `no-permissions` keyword can be used:
```
Note-PCRLT:j..e.4:copyright-holder-add-remove-modify-group-policy:no-permissions,conditions:personal-data-policy
```
### 5.3 Copyright Holder Specification in License Files
##### 5.3.1 The Keyword: "copyright-holder-in-license-files-add-remove-modify-group-policy"
This is an experimental feature and more thought needs to be given to its use and usefulness before it can be recommended.  
This is similar to 5.2.5 but controls the copyright notice in separate license files (if any).  
Example:
```
Note:j..e.4:copyright-holder-in-license-files-add-remove-modify-group-policy:add-allowed,remove-allowed,modify-allowed,group-allowed,conditions:personal-data-policy
```
group-allowed means that the copyright holder permits to be subsumed in a grouping copyright notice like:
```
SPDX-FileCopyrightText:Developers of Project XYZ
```
The `conditions:personal-data-policy` is redundant, personal-data-policy applies in any case. This is just to avoid mistakes.  
If no permission should be granted (this is the default) the `no-permissions` keyword can be used:
```
Note-PCRLT:j..e.4:copyright-holder-in-license-files-add-remove-modify-group-policy:no-permissions,conditions:personal-data-policy
```
## 6 License Tracing
### 6.1 The Keywords: "map-license-to-git-commits", "license-background"
The keyword `map-license-to-git-commits` is analog to `map-copyright-holder-to-git-commits`.  
Example:
```
SPDX-FileCopyrightText: 2015 j..e.4
SPDX-License-Identifier: MIT
Note-PCRLT:j..e.4:map-copyright-holder-to-git-commits:all commits by j..e.4
Note-PCRLT:MIT:map-license-to-git-commits:all commits by j..e.4
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
```
SPDX-FileCopyrightText: 2015 Future Inc.
SPDX-License-Identifier: CC-BY-ND-4.0
Note-PCRLT:MIT:license-background:The license conditions are stated on https://somesite1. License tag was set by j..e.4 (it might be less restrictive). 
Note-PCRLT:j..e.4:resolve-git-identity:https://repository1
```
## 7 Compliance Notices and Miscellaneous
### 7.1 Compliance Notices
Example:
```
Note-PCRLT:compliant-with-spec:SPDXv2.2:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.3:https://gitlab.com/emb_std/pcrlt
```
### 7.2 Multiline Text Values
If the text value contains multiple lines it needs to be enclosed by \<text\>\</text\>. It is unclear whether indentations without text tags will be supported in the future (currently they are not supported). Multiline text values are often needed for: author-background, copyright-background, license-background, map-copyright-holder-to-git-commits, map-license-to-git-commits. Line breaks in URLs are currently not supported, but might be supported in the future. 

### 7.3 Multiple Identity-Reference-Ids concatenated with "AND" as short Notation
Multiple Identity-Reference-Ids can be concatenated with "AND" for the keywords map-copyright-holder-to-git-commits, map-author-to-git-commits, copyright-background, author-background.

For map-copyright-holder-to-git-commits it can be used to specify commits that contain snippets copyrighted by multiple holders (either as joint work or each snippet by one rights holder). This is especially helpful if this overlapping/intersection condition would be surprisingly otherwise.  
Consider the following example:

```
SPDX-FileCopyrightText: 2016 j..e.4
SPDX-FileCopyrightText: 2015-2016 Future Inc.
Note-PCRLT:F.I.:specify-identity-name:Future Inc.
Note-PCRLT:F.I.:map-copyright-holder-to-git-commits:<text>commits authored/committed by j..e.4 
  between (incl.) 2015-01-01 and (incl.) 2016-01-15 regarding file f1.txt
  (first:1234567 2015-01-15,last:1234568 2016-01-15)</text>
Note-PCRLT:j..e.4:map-copyright-holder-to-git-commits:<text>commits authored/committed by j..e.4 
  after (incl.) 2016-01-01 regarding file f1.txt
  (first:1234569 2015-01-01)</text>

Note-PCRLT:F.I. AND j..e.4:map-copyright-holder-to-git-commits:<text>commits authored/committed by j..e.4 
  between (incl.) 2016-01-01 and (incl.) 2016-01-15 regarding file f1.txt 
  (first:1234569 2015-01-01,last:1234568 2016-01-15)</text>
Note-PCRLT:F.I. AND j..e.4:copyright-background:<text> j..e.4 was employed at Future Inc. from 2015-01-01 to 2016-01-15, 
  some commits contain snippets copyrighted by F.I. as well as snippets copyrighted by j..e.4
```
With the copyright-background note the reason can be explained or further helpful information can be provided.

### 7.4 dep5 Annotations
The standard place for REUSE/PCRLT file headers are the earliest possible block as a (format dependent) comment in a text file (see REUSE spec).  
REUSE also allows to place annotations in .license files and the .reuse/dep5 file. This is also possible for PCRLT. If the dep5 file is used some special rules apply.

#### 7.4.1 File Header Declarations in the dep5 File
The dep5 file allows to specify copyright and license information for multiple files - either by wildcard or by a list of files. To nevertheless specify PCRLT annotation for each file it is possible to specify PCRLT file headers below the dep5 policy section for the set of files.  
Each PCRLT file header starts with `Note-PCRLT:begin-file-header-for:` and ends with `Note-PCRLT:end-file-header-for:` followed by the filename (with path).

#### 7.4.2 File Header Declaration for the dep5 File
PCRLT annotations before any dep5 policy expressions are interpreted as annotations for the .reuse/dep5 file itself.

#### 7.4.3 Don't repeat Compliance Notes
To avoid repeating, PCRLT compliance notes (`compliant-with-spec:`) have to be placed only in the .reuse/dep5 file header.

### 7.5 Current Git Version And Git History
PCRLT annotations are primarily intended to provide copyright and license information for the current Git version. Mappings to Git commits are intended to provide information for content snippets. Only those parts of the Git history that share content with the current version are targeted by PCRLT-annotations.  
Another issue is the licensing of content from the Git history that is not part of the current Git Version. PCRLT annotations for replaced content is possible if an identically named file exists in the current version, but is not recommended. Copyright and license information for the git history (if the information is incomplete or incorrect in the particular version) should be supplied elsewhere - e.g. as part of a LICENSE_README.md file.  
Note that REUSE annotations describe exclusively the current Git version (Copyright holders of fully replaced content should not be listed with `SPDX-FileCopyrightText` annotation, but a PCRLT `map-copyright-holder-to-git-commits` is allowed for them).

### 7.6 Levels/Badges: Bronze, Silver, Gold
To easily show and iteratively increase the amount of copyright and license information, currently 3 levels with corresponding badges can be used (Bronze, Silver, Gold). To use the badges, the corresponding PCRLT requirements must be met - with Bronze essentially requiring only REUSEv3.0 requirements. Each level includes the requirements of the lower levels.
Level/badge requirements can target the individual files (or more precisely their corresponding fileheaders) in the repository or the repository as a whole.  
Fileheader requirements (using the possible fileheader types: std, .license, dep5):  
For level 1 (Bronze):  
* The file needs at least one Rights Holder specification with the "SPDX-FileCopyrightText:" keyword.
* The file needs at least one License Expression specification with the "SPDX-License-Identifier:" keyword.

For level 2 (Silver):  
* The file has "compliant-with-spec:" annotations specifying the targeted SPDX, REUSE and PCRLT versions.
* If the file has more than one Rights Holder specifications, there need to be eighter:
    * separate snippets with one Rights Holder per each snippet,
    * or each Rights Holder has a commit mapping using the "map-copyright-holder-to-git-commits:" keyword.
* If the file has more than one License specifications (i.e. not connected with "AND" or "OR"), there need to be eighter:
    * separate snippets with one License Expression per each snippet,
    * or each License Expression has a commit mapping using the "map-license-to-git-commits:" keyword.
    
For level 3 (Gold):  
* If the Rights Holder specifications contains no contact and the name has less than 8 characters, 
the identity is linked to a git identity using the "resolve-git-identity:" keyword.
* The file contains at least one "src-trace-repository:" annotation, where the mentioned filepath matches the current file path and the url matches the dep5 source url.

Repository requirements:  
For a repository to meet level x requirements, all files (except in the .git folder, but including the LICENSES folder) in the repository must meet the file-based requirements of that level x. In addition, the REUSEv3.0 requirements must be met (i.e., the LICENSES folder is present and contains all referenced licenses, the .reuse/dep5 file is present).

### 7.7 The Keyword "external-annotations"
If a file is used in multiple repositories, the annotations can be specified in one repository (A) and in another repository (B) the keyword "external-annotations" can be used to refer to these external annotations. The targeted use case is license files. Suppose the file LICENSES/CC0-1.0.txt can be found in an external repository (A) at git+https://gitlab.com/emb_std/pcrlt-licenses.git#LICENSES/CC-BY-4.0.txt with a dep5 file header, these annotations can be referenced in the .reuse/dep5 file (of repository B) with the following annotation:
```
Files: LICENSES/CC-BY-4.0.txt
Copyright: 2002-2013 Creative Commons Corporation <creativecommons.org>
License: CC0-1.0
# Note-PCRLT:begin-file-header-for: LICENSES/CC-BY-4.0.txt
# Note-PCRLT:external-annotations:sha1:42d6494d51317826789dc2064c6440f0d4448376:dep5:https://gitlab.com/emb_std/pcrlt-licenses.git#LICENSES/CC-BY-4.0.txt
# Note-PCRLT:end-file-header-for: LICENSES/CC-BY-4.0.txt
```
The sha1 sum refers to the output of "sha1sum LICENSES/CC-BY-4.0.txt" (in repository A and B). "dep5" refers to the file header type in A.  
If the copyright and license information in repository A changes (with respect to the target file), the available copyright and license information in repository B takes precedence. The additional PCRLT annotations in A apply to B, if applicable. If the contents of the file in A change, the commit that refers to the specified file hash must be determined to determine the annotations that apply to that version. The external-annotations keyword shall not be used for unstable files and only if the level 3 requirements in A are met for the file. (It is not expected that tools support parsing of external annotations, but instead simply assume level 3 for them and note this assumption in the report/log.)

## 8 Basic Use Cases and Examples
### 8.1 Use Case: Add a Copyright Notice
* Ideally, the copyright holders write and commit the copyright notice themselves, or carefully instruct someone to do so.
* If there is no file-based copyright notice, but the file is included in a repository with a license that includes copyright holders (such as MIT, BSD) and there is no indication that the file is licensed differently, then it seems reasonable to copy the copyright holders from the license into the file header. However, it is possible that not all copyright holders from the license hold copyrights to the file in question.
* Using the git history to determine the copyright holders does not seem practical, since the authors may be employed and then the employer usually holds the copyright. However, it might still be helpful to write the authors into the file, since mostly asking authors are the best way to find the copyright holders. If the content files does not contain any copyright-holder or author references but there are author references in the git history and the repository is public, the proposal would be to assume that references in abbreviated form are accaptable for the authors (from a privacy perspective).
Suppose the git commits show authorship by "john"\<johnd@example.com\> and/or "jane"\<janed@example.com\>, then the content could be enriched with the following:
```
Note-PCRLT:authors:<text>
    2016-2018 j..n.4
    2015 j..e.4</text>
Note-PCRLT:j..n.4:resolve-git-identity-url:https://repository1
Note-PCRLT:j..e.4:resolve-git-identity-url:https://repository1
```
If the copyright holders are unknown, it might nevertheless help to specify this using the following syntax:
```
SPDX-FileCopyrightText: 2016-2018 Unknown (author: j..n.4)
SPDX-FileCopyrightText: 2015 Unknown (author: j..e.4)
SPDX-License-Identifier: NoAssertion
Note-PCRLT:authors:<text>
    2016-2018 j..n.4
    2015 j..e.4</text>
Note-PCRLT:j..n.4:resolve-git-identity-url:https://repository1
Note-PCRLT:j..e.4:resolve-git-identity-url:https://repository1
```
* If j..n.4 is sure that he holds the copyright for his changes, he should clarify this (He could omit the background note, but it will help reduce any doubt; Here it is assumed he will license his commits under the MIT license):
```
SPDX-FileCopyrightText: 2016-2018 j..n.4
SPDX-FileCopyrightText: 2015 Unknown (author: j..e.4)
SPDX-License-Identifier: MIT
SPDX-License-Identifier: NoAssertion
Note-PCRLT:j..n.4:copyright-background:"2016-2018 j..n.4" was added by j..n.4 (self) because no employer or copyright-assignement was involved.
Note-PCRLT:j..n.4:map-copyright-holder-to-git-commits:commits by j..n.4
Note-PCRLT:MIT:map-license-to-git-commits:commits by j..n.4
Note-PCRLT:NoAssertion:map-license-to-git-commits:commits by j..e.4
Note-PCRLT:authors:<text>
    2016-2018 j..n.4
    2015 j..e.4</text>
Note-PCRLT:j..n.4:resolve-git-identity-url:https://repository1
Note-PCRLT:j..e.4:resolve-git-identity-url:https://repository1
```
* If j..n.4 has good reason to think, that an author or employer is the copyright holder he may add the following:
```
SPDX-FileCopyrightText: 2015 j..e.4
Note-PCRLT:j..e.4:copyright-background:"2015 j..e.4" was added by j..n.4 because j..e.4 said that no employer or copyright-assignement was involved.
```
* If j..n.4 is not sure if j..e.4 is employed he might add:
```
SPDX-FileCopyrightText: 2015 j..e.4 OR some employer
Note-PCRLT:j..e.4:copyright-background:"2015 j..e.4" was added by j..n.4 because j..e.4 said that no copyright-assignement was involved but j..e.4 was not sure about involvement of employer and which employer.
```
* If the copyright holder is actually an employer and no license was granted by the employer, this could be a copyright violation. And if this is the case, it could be that j..e.4 was not allowed to commit in the first place. The risk that infringement could occur should not discourage proper documentation - even though proper documentation may increase the risk of rights holders enforcing their rights.

### 8.2 Use Case: Remove, Modify, Group a Copyright Notice
Normally, a copyright notice should not be removed or modified except by the copyright holder himself/herself or someone authorized by the holder. The copyright notice can be removed if the content is completely replaced (note that replacing a line does not mean that the content of that line has been completely replaced). If the copyright notice is incorrect, the notice can be corrected provided that no additional personal data is added. Leave a note as to why this was done and by whom. If a policy is in place (see 5.2.1.1), the rule not to touch the copyright notice can be weakened.  
Suppose the following file header (part) is given:
```
SPDX-FileCopyrightText: 2016-2018 j..n.4
SPDX-FileCopyrightText: 2015 j..e.4 OR some employer
Note-PCRLT:j..e.4:copyright-background:"2015 j..e.4" was added by j..n.4 because j..e.4 said that no copyright-assignement was involved but j..e.4 was not sure about involvement of employer and which employer.
```
Now maybe j..e.4 has written a mail to j..n.4 stating that j..e.4 found some notes indicating the employer were "Futur Inc.", but the employer had agreed to the inbound=outbound license rule i.e. a MIT license exists for these commits. j..n.4 could change the lines as follows:
```
SPDX-FileCopyrightText: 2016-2018 j..n.4
SPDX-FileCopyrightText: 2015 Future Inc.
SPDX-License-Identifier: MIT
Note-PCRLT:j..n.4:copyright-background:"2016-2018 j..n.4" was added by j..n.4 (self) because no employer or copyright-assignement was involved.
Note-PCRLT:Futur Inc.:copyright-background:"2015 Future Inc." was added by j..n.4 because j..e.4 was employed there at this time. This was licensed under MIT.
Note-PCRLT:j..n.4:map-copyright-holder-to-git-commits:commits by j..n.4
Note-PCRLT:Future Inc.:map-copyright-holder-to-git-commits:commits by j..e.4

Note-PCRLT:MIT:map-license-to-git-commits:commits by j..n.4 OR j..e.4

Note-PCRLT:authors:<text>
    2016-2018 j..n.4
    2015 j..e.4</text>
Note-PCRLT:j..n.4:resolve-git-identity-url:https://repository1
Note-PCRLT:j..e.4:resolve-git-identity-url:https://repository1
```

#### 8.4 Use Case: rights-holder or author references does not match with commits
If the content files does contain copyright-holder or author references but they do not match with git commits from the corresponding public git repository, the authors should link those references in abbreviated form to the git identity and if the others are sure about they may add those links too (if this will not link identities the user wants to keep separate).  
Suppose the content file shows the copyright entry `Copyright: 2015 Jane Doe` and git commits show authorship by "janed"\<janed@example.com\> and You are sure they refere to the same natural person and the person has consented to link those identities, then the content could be enriched with the following:
```
SPDX-FileCopyrightText: 2015 Jane Doe
SPDX-License-Identifier: MIT
Note-PCRLT:J..e.8:specify-identity:Jane Doe
Note-PCRLT:J..e.8:map-to-git-identity:j..d.5
Note-PCRLT:j..d.5:resolve-git-identity:https://repository1
```
or Jane Doe might write
```
SPDX-FileCopyrightText: 2015 Jane Doe
SPDX-License-Identifier: MIT
Note-PCRLT:Jane Doe:map-to-git-identity:j..d.5
Note-PCRLT:j..d.5:resolve-git-identity:https://repository1
```
or
```
SPDX-FileCopyrightText: 2015 Jane Doe
SPDX-License-Identifier: MIT
Note-PCRLT:Jane Doe:map-to-git-identity:janed
Note-PCRLT:janed:resolve-git-identity:https://repository1
```
#### 8.5 Use Case: multiple identities
If users use muliple identities, they should not be linked to each other. It is ok, to list the same author multiple times if he/she uses different identities/names/email-addresses.  
Suppose the git commits show authorship by "janed"\<janed@example.com\> and "janes"\<janes@example.com\> and you know that it links to the same natural person (may be because of marriage), it should still be listet as:
```
Note-PCRLT:authors:<text>
    2015 j..d.5
    2016-2018 j..s.5</text>
Note-PCRLT:j..d.5:resolve-git-identity:https://repository1
Note-PCRLT:j..s.5:resolve-git-identity:https://repository1
```
or she (as author) might write:
```
SPDX-FileCopyrightText: 2016-2018 Jane Smith
SPDX-FileCopyrightText: 2015 Jane Doe
SPDX-License-Identifier: MIT
Note-PCRLT:Jane Smith:map-to-git-identity:j..s.5
Note-PCRLT:Jane Doe:map-to-git-identity:j..d.5
Note-PCRLT:j..d.5:resolve-git-identity:https://repository1
Note-PCRLT:j..s.5:resolve-git-identity:https://repository1
```
or if she has not added a new git user identity and want to still use "janed"\<janed@example.com\>, she might write:
```
SPDX-FileCopyrightText: 2016-2018 Jane Smith
SPDX-FileCopyrightText: 2015 Jane Doe
SPDX-License-Identifier: MIT
Note-PCRLT:Jane Smith:map-to-git-identity:j..d.5
Note-PCRLT:Jane Doe:map-to-git-identity:j..d.5
Note-PCRLT:j..d.5:resolve-git-identity:https://repository1
```
or she could merge her former entry:
```
SPDX-FileCopyrightText: 2015-2018 Jane Smith
SPDX-License-Identifier: MIT
Note-PCRLT:Jane Smith:map-to-git-identity:j..d.5
Note-PCRLT:j..d.5:resolve-git-identity:https://repository1
```

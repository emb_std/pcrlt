# SPDX-FileCopyrightText: 2021-2022 embeach
# 
# SPDX-License-Identifier: CC0-1.0
# 
# Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
# Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
# Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt
# 
# Note-PCRLT:src-trace-repository:2022:public:git+https://gitlab.com/emb_std/pcrlt.git@master#Makefile
# Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/spdx-utils.git@master#Makefile
# 
# Note-PCRLT:emb.7:specify-identity-name:embeach
# Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt.git@master#Makefile
# Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits authored/committed by emb.7 regarding Makefile
# 
# Note-PCRLT:CC0-1.0:map-license-to-git-commits:same as "emb.7:map-copyright-holder-to-git-commits"

download-spdx-utils-main:
	if [ -d "spdx-utils-main" ]; then echo "The folder already exists. A download does not seem to be necessary." && false ; fi
	#clone the spdx-utils repository (if spdx-utils-main does not exists)
	git clone https://gitlab.com/emb_std/spdx-utils.git tmp_spdx-utils
	#checkout specific tag version
	cd tmp_spdx-utils && git checkout tags/v0.1.4
	#copy the needed main folder
	cp -a tmp_spdx-utils/spdx-utils-main spdx-utils-main
	#the following will copy the spdx-utils config into the spdx-utils-main folder as spdx-utils.version to give a hint about the original used spdx-utils version
	cp tmp_spdx-utils/spdx-utils-input/config spdx-utils-main/spdx-utils.version
	#remove the repository
	rm -rf tmp_spdx-utils

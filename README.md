<!--
SPDX-FileCopyrightText: 2021-2022 embeach

SPDX-License-Identifier: CC-BY-SA-4.0

Note-PCRLT:compliant-with-spec:SPDXv2.3:https://spdx.github.io/spdx-spec/
Note-PCRLT:compliant-with-spec:REUSEv3.0:https://reuse.software/spec/
Note-PCRLT:compliant-with-spec:PCRLTv0.4:https://gitlab.com/emb_std/pcrlt

Note-PCRLT:src-trace-repository:2021:public:git+https://gitlab.com/emb_std/pcrlt.git@master#README.md

Note-PCRLT:emb.7:specify-identity-name:embeach
Note-PCRLT:emb.7:resolve-git-identity:git+https://gitlab.com/emb_std/pcrlt.git@master#README.md
Note-PCRLT:emb.7:map-copyright-holder-to-git-commits:all commits

Note-PCRLT:CC-BY-SA-4.0:map-license-to-git-commits:all commits
-->
# PCRLT - README
This repository is the original host for the document PCRLT.md at the URL https://gitlab.com/emb_std/pcrlt .  
**PCRLT stands for Privacy-aware Content, Rights Holder and License Tracing.**  
The PCRLT.md document is intended to provide guidance for privacy-aware, reliably and accurately documenting and managing rights holders and license terms for all content in an online Git repository. In addition, it represents a referenceable standard that should mitigate legal risks. There are already standards by SPDX[^spdx] and REUSE[^reuse]. PCRLT[^pcrlt] is intended to complement them.  
## Main Features
1. compliant with REUSEv3.0
2. map copright holders to commits
3. map licenses to commits
4. map copyright holders or authors to git identities in a privacy-compliant manner (by using an abbreviation/hashing technique)
5. trace contents by providing URLs
6. increase confidence in copyright and license declarations by specifying accountability and evidence for such claims
7. flexible use of PCRLT annotations by using 3 levels/badges (Bronze, Silver, Gold), with Bronze essentially requiring only REUSEv3.0 requirements
8. comfortable tool support is available (see Tool Support)
9. specify personal attribution policies (privacy, copyright) to allow greater flexibility for later contributors (experimental feature)

## Published Versions
* 2022-12-04 v0.4: https://gitlab.com/emb_std/pcrlt/-/blob/v0.4/PCRLT.md
* 2022-10-03 v0.3: https://gitlab.com/emb_std/pcrlt/-/blob/v0.3/PCRLT.md
* 2021-04-20 v0.2: https://gitlab.com/emb_std/pcrlt/-/blob/v0.2/PCRLT.md
* 2021-04-14 v0.1: https://gitlab.com/emb_std/pcrlt/-/blob/v0.1/PCRLT.md
## History/Background
PCRLT was developed because the REUSE specification currently (2022-11) does not provide sufficient expressive power to specify copyright and license information in a detailed and privacy-compliant manner.
## Tool Support
The pcrlt-validator[^pcrlt-validator] is able to validate most of the current PCRLT spec and supports the compliance process with many features.
## License
The Project is SPDXv2.3[^spdx], REUSEv3.0[^reuse] and PCRLTv0.4[^pcrlt] compliant - i.e. copyright and license information are provided on a per-file basis. A summary is provided in LICENSE.spdx as SPDX document. Full license texts can be found in the LICENSES folder.  

The repository contains references to the following licenses: CC-BY-SA-4.0, CC0-1.0  
The SPDX PackageLicenseConcluded as well as PackageLicenseDeclared is: CC-BY-SA-4.0[^cc-by-sa-4]  
Note: This is not an independent normative statement, but instead is the package license based on the license information of the individual files.  
See LICENSE.spdx for further details.

## References
[^spdx]:SPDX (Software Package Data Exchange): https://spdx.dev  
[^reuse]:REUSE (fsfe-reuse) project: https://reuse.software  
[^pcrlt]:PCRLT (Privacy-aware Content, Rights Holder and License Tracing): https://gitlab.com/emb_std/pcrlt  
[^cc-by-sa-4]:Creative Commons Share Alike 4.0 License (CC-BY-SA-4.0):https://creativecommons.org/licenses/by-sa/4.0/  
[^pcrlt-validator]:pcrlt-validator: https://gitlab.com/emb_std/pcrlt-validator
